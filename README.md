Multilayer AutoEncoder implmenetation in python using the Tensorflow API. Implements several regularization strategies:

* Contraction
* Dimensionality restricted contraction
* Orthogonal Contraction 
* Denoising